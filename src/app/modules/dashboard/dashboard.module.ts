import { NgModule } from '@angular/core';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { DashboardRoutingModule } from './routes/dashboard-routing.module';
import { SharedModule } from '@app//shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [DashboardComponent],
    imports: [
        RouterModule,
        DashboardRoutingModule,
        SharedModule,
    ]
})
export class DashboardModule { }
