import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/User';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) { }

    getUsers() {
        return this.http.get<any>(`user/users/index`);
    }

    storeUser(user: User) {
        return this.http.post<any>(`user/users/store`, user);
    }

    updateUser(user: User) {
        return this.http.post<any>(`user/users/update`, user);
    }

    findUser(id: string) {
        return this.http.get<any>(`user/users/edit`, {params: {id}});
    }

    resetPassword(user: User) {
        return this.http.post<any>(`user/users/change-password`, user);
    }
}
