import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@app//modules/authentication/guards/auth.guard';
import { IndexComponent } from './views/users/index/index.component';
import { CreateComponent } from './views/users/create/create.component';
import { EditComponent } from './views/users/edit/edit.component';

const routes: Routes = [
    {
        path: 'users',
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                component: IndexComponent,
            },
            {
                path: 'create',
                component: CreateComponent,
            },
            {
                path: 'edit/:id',
                component: EditComponent,
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class UserRoutingModule {

}