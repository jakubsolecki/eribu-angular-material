import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app//shared/shared.module';
import { UserRoutingModule } from './user-routing.module';

import { IndexComponent } from './views/users/index/index.component';
import { CreateComponent } from './views/users/create/create.component';
import { EditComponent } from './views/users/edit/edit.component';

@NgModule({
  declarations: [
      IndexComponent,
      CreateComponent,
      EditComponent,
  ],
  imports: [
    RouterModule,
    SharedModule,
    UserRoutingModule,
  ]
})
export class UserModule { }
