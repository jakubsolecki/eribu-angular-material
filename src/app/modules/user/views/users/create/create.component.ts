import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FormErrorsMatcher } from '@app/shared/libs/FormErrorsMatcher';
import { UserService } from '@app/modules/user/services/user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateComponent implements OnInit {
    form: FormGroup;
    matcher = new FormErrorsMatcher();

    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private router: Router,
        private changeDetectorRef: ChangeDetectorRef,
        ) { }

    ngOnInit() {
        this.createForm();
    }

    createForm() {
        this.form = this.fb.group({
            firstname: [''],
            lastname: [''],
            email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
            password: ['', [Validators.required]],
            password_confirm: ['', [Validators.required]],
        });
    }
    onSubmit() {
        if (this.form.valid) {
            this.userService.storeUser(this.form.value).subscribe(res => {
                this.changeDetectorRef.detectChanges();
                console.log(res, 'res');
                this.router.navigate(['/user/users']);
            }, err => {
                console.log(err, 'err');
            });

        }
    }

}
