import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { FormErrorsMatcher } from '@app/shared/libs/FormErrorsMatcher';
import { UserService } from '@app/modules/user/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '@app/modules/user/models/User';
import _pick from 'lodash/pick';
import _keys from 'lodash/keys';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditComponent implements OnInit {
    form: FormGroup;
    resetPassForm: FormGroup;

    matcher = new FormErrorsMatcher();

    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private fb: FormBuilder,
        private userService: UserService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.createForm();
        this.createResetPassForm();
        this.userService.findUser(this.route.snapshot.params.id).subscribe((data: { status: string, model: User }) => {
            this.form.setValue(_pick(data.model, _keys(this.form.value)));
        });
    }

    createForm() {
        this.form = this.fb.group({
            firstname: [''],
            lastname: [''],
            email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        });
    }
    onSubmit() {
        if (this.form.valid) {
            this.userService.updateUser({
                _id: this.route.snapshot.params.id,
                ...this.form.value,
            }).subscribe(res => {
                console.log(res, 'res');
                this.changeDetectorRef.detectChanges();
                this.router.navigate(['/user/users']);
            }, err => {
                console.log(err, 'err');
            });

        }
    }
    // reset password form
    createResetPassForm() {
        this.resetPassForm = this.fb.group({
            password: ['', [Validators.required]],
            password_confirm: ['', [Validators.required]],
        });
    }
    resetPassSubmit() {
        if (this.resetPassForm.valid) {
            console.log('reset pass');
            this.userService.resetPassword(
                {
                    _id: this.route.snapshot.params.id,
                    ...this.resetPassForm.value
                }
            ).subscribe(res => {
                console.log(res, 'res');
                this.changeDetectorRef.detectChanges();
                this.router.navigate(['/user/users']);
            }, err => {
                console.log(err, 'err');
            });
        }
    }
}
