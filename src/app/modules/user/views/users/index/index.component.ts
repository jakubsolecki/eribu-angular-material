import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from '../../../models/User';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,

})
export class IndexComponent implements OnInit {
    displayedColumns: string[] = ['_id', 'firstname', 'lastname', 'email', 'createdAt'];
    dataSource: MatTableDataSource<User>;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    constructor(
        private userService: UserService,
        private changeDetectorRef: ChangeDetectorRef,
        ) {
        this.dataSource = new MatTableDataSource([]);
    }

    ngOnInit() {
        this.userService.getUsers().subscribe( res => {
            this.dataSource = new MatTableDataSource(res.results);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.changeDetectorRef.detectChanges();
        }, err => {
            console.log(err, 'res');
        });

    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

}
