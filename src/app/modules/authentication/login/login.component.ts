import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { FormErrorsMatcher } from '../../../shared/libs/FormErrorsMatcher';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    matcher = new FormErrorsMatcher();

    constructor(private fb: FormBuilder, private auth: AuthenticationService, private router: Router) { }

    ngOnInit() {
        this.createForm();
    }
    onSubmit() {

        if (this.loginForm.valid) {
            console.log(this.loginForm.value, 'this.loginForm.value');
            this.auth.login(this.loginForm.value)
                .subscribe(res => {
                    console.log(res, 'res');
                    this.router.navigate(['/']);
                },
                error => {
                    console.log(error, 'error');
                });
        }
    }

    createForm() {
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
            password: ['', [Validators.required]],
            remember_me: [''],
        });
    }

}
