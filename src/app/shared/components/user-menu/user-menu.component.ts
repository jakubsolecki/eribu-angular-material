import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app//modules/authentication/services/authentication.service';
import { User } from '@app//modules/authentication/models/User';
import { Router } from '@angular/router';

@Component({
    selector: 'app-user-menu',
    templateUrl: './user-menu.component.html',
    styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {

    user: User;
    constructor(private auth: AuthenticationService, private router: Router) { }

    ngOnInit() {
        this.user = this.auth.currentUserValue;
    }

    logout() {
        this.auth.logout();
        this.router.navigate(['/auth/login']);
    }

}
