import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';


import { MasterComponent } from '@app//shared/layout/master/master.component';
import { HeaderComponent } from '@app//shared/layout/master/header/header.component';
import { SidebarComponent } from '@app//shared/layout/master/sidebar/sidebar.component';
// material
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material';
import { MatSortModule } from '@angular/material/sort';
import { MatExpansionModule } from '@angular/material/expansion';

import { UserMenuComponent } from './components/user-menu/user-menu.component';


@NgModule({
    declarations: [
        MasterComponent,
        HeaderComponent,
        SidebarComponent,
        UserMenuComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,

        MatIconModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatTabsModule,
        MatCheckboxModule,
        MatGridListModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatExpansionModule,
    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,
        MasterComponent,
        HeaderComponent,
        SidebarComponent,

        MatIconModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatTabsModule,
        MatCheckboxModule,
        MatGridListModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatExpansionModule,
    ]
})
export class SharedModule { }
